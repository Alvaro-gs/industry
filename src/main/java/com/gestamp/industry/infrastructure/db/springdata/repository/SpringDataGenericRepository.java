package com.gestamp.industry.infrastructure.db.springdata.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface SpringDataGenericRepository<T, K> extends JpaRepository<T, K> {

  Page<T> findAll(Specification<T> spec, Pageable pageable);

}
