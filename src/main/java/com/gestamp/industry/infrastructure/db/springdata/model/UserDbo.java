package com.gestamp.industry.infrastructure.db.springdata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "user")
public class UserDbo {

    @Id
    @SequenceGenerator(name = "user_id_seq",
            sequenceName = "user_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "user_id_seq")
    private long id;

    private String username;

    private String password;


}
