package com.gestamp.industry.infrastructure.db.springdata.mapper;

import com.gestamp.industry.domain.UserToken;
import com.gestamp.industry.infrastructure.db.springdata.model.UserDbo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserToken toDomain(UserDbo dbo);

    UserDbo toInfrastructure(UserToken entity);

}
