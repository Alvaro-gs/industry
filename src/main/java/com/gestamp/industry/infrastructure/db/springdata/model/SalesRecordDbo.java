package com.gestamp.industry.infrastructure.db.springdata.model;

import com.gestamp.industry.domain.OrderPriorityEnum;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "sales_record", indexes = {
    @Index(columnList = "country"),
    @Index(name = "fn_index", columnList = "country"),
    @Index(name = "mulitIndex1", columnList = "region, country"),
    @Index(name = "mulitIndex2", columnList = "country, sales_channel")})
public class SalesRecordDbo {

    @Id
    @SequenceGenerator(name = "sales_id_seq",
            sequenceName = "sales_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "sales_id_seq")
    private Long id;

    @Column(name = "region")
    private String region;

    @Column(name = "country")
    private String country;

    @Column(name = "item_type")
    private String itemType;

    @Column(name = "sales_channel")
    private String salesChannel;

    @Column(name = "order_priority")
    @Enumerated(EnumType.STRING)
    private OrderPriorityEnum orderPriority;

    @Column(name = "order_date")
    private String orderDate;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "ship_date")
    private LocalDate shipDate;

    @Column(name = "unit_sold")
    private Long unitSold;

    @Column(name = "unit_price")
    private Float unitPrice;

    @Column(name = "unit_cost")
    private Float unitCost;

    @Column(name = "total_revenue")
    private Double totalRevenue;

    @Column(name = "total_cost")
    private Double totalCost;

    @Column(name = "total_profit")
    private Double totalProfit;

}
