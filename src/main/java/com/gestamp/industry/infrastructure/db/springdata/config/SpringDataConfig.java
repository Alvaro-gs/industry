package com.gestamp.industry.infrastructure.db.springdata.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.gestamp.industry.infrastructure.db.springdata.repository")
@EntityScan(basePackages = "com.gestamp.industry.infrastructure.db.springdata.model")
@RequiredArgsConstructor
public class SpringDataConfig {

}
