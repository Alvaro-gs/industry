package com.gestamp.industry.infrastructure.db.springdata.repository;

import com.gestamp.industry.infrastructure.db.springdata.model.UserDbo;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringDataUserRepository extends SpringDataGenericRepository<UserDbo, Long> {

    UserDbo findByUsername(String username);

}
