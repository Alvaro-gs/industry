package com.gestamp.industry.infrastructure.db.springdata.mapper;

import com.gestamp.industry.domain.filter.SaleRecordFilter;
import com.gestamp.industry.infrastructure.db.springdata.model.SalesRecordDbo;
import com.gestamp.industry.infrastructure.db.springdata.model.SalesRecordDbo_;
import com.google.common.base.Strings;
import org.springframework.stereotype.Component;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;

@Component
public class SaleRecordFilterMapper {

    public Specification<SalesRecordDbo> toSpec(SaleRecordFilter filter) {
        return (Specification<SalesRecordDbo>) (timetableRoot, query, cb) -> {

            List<Predicate> predicates = new ArrayList<>();

            if(filter.getId() != null ) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.id), filter.getId()));
            }

            if(!Strings.isNullOrEmpty(filter.getCountry())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.country), filter.getCountry()));
            }

            if(!Strings.isNullOrEmpty(filter.getRegion())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.region), filter.getRegion()));
            }

            if(!Strings.isNullOrEmpty(filter.getItemType())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.itemType), filter.getItemType()));
            }

            if(!Strings.isNullOrEmpty(filter.getOrderDate())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.orderDate), filter.getOrderDate()));
            }

            if(!Strings.isNullOrEmpty(filter.getSalesChannel())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.salesChannel), filter.getSalesChannel()));
            }

            if(!Strings.isNullOrEmpty(filter.getShipDate())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.shipDate), filter.getShipDate()));
            }

            if(!Strings.isNullOrEmpty(filter.getOrderPriority())) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.orderPriority), filter.getOrderPriority()));
            }

            if(filter.getOrderId() != null ) {

                predicates.add(cb.equal(timetableRoot.get(SalesRecordDbo_.orderId), filter.getOrderId()));
            }

            query.distinct(true);

            return cb.and(predicates.toArray(Predicate[]::new));
        };
    }
}
