package com.gestamp.industry.infrastructure.db.springdata.repository.impl;

import com.gestamp.industry.domain.SalesRecord;
import com.gestamp.industry.application.repository.SalesRecordRepository;
import com.gestamp.industry.domain.filter.SaleRecordFilter;
import com.gestamp.industry.domain.pagination.PageabledResult;
import com.gestamp.industry.domain.pagination.Pagination;
import com.gestamp.industry.domain.pagination.PaginationInfo;
import com.gestamp.industry.infrastructure.db.springdata.mapper.SaleRecordFilterMapper;
import com.gestamp.industry.infrastructure.db.springdata.mapper.SalesRecordMapper;
import com.gestamp.industry.infrastructure.db.springdata.mapper.pagination.PaginationMapper;
import com.gestamp.industry.infrastructure.db.springdata.model.SalesRecordDbo;
import com.gestamp.industry.infrastructure.db.springdata.repository.SpringDataSalesRecordRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class SalesRecordRepositoryImpl implements SalesRecordRepository {

    private final SpringDataSalesRecordRepository salesRecordRepository;

    private final SalesRecordMapper mapper;

    private  SaleRecordFilterMapper filterEspecification;

    @Override
    public SalesRecord saveAndUpdateSalesRecord(SalesRecord salesRecord) {
        return mapper.toDomain(salesRecordRepository.save(mapper.toInfrastructure(salesRecord)));
    }

    @Override
    public void deleteSalesRecord(Long id) {
         salesRecordRepository.deleteById(id);
    }

    @Override
    public PageabledResult<SalesRecord> findByFilter(SaleRecordFilter filter, Pagination pagination) {

        Specification<SalesRecordDbo> specs = filterEspecification.toSpec(filter);

        Pageable pageable = PaginationMapper.toPageable(pagination);

        Page<SalesRecordDbo> page = salesRecordRepository.findAll(specs, pageable);

        PageabledResult<SalesRecord> pageabledResult = new PageabledResult<>();

        pageabledResult
                .setContent(page.getContent().stream().map(dbo -> mapper.toDomain(dbo)).collect(Collectors.toList()));

        pageabledResult.setPaginationInfo(new PaginationInfo(page.getNumber(), page.getSize(), page, pagination.getSortBy()));

        return pageabledResult;
    }

    @Override
    public Float totalCost(String country) {
        return salesRecordRepository.totalCost(country);
    }


}
