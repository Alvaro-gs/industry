package com.gestamp.industry.infrastructure.db.springdata.repository;

import com.gestamp.industry.infrastructure.db.springdata.model.SalesRecordDbo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringDataSalesRecordRepository extends SpringDataGenericRepository<SalesRecordDbo, Long> {

    void deleteById(Long id);

    @Query("SELECT SUM(m.totalCost) FROM SalesRecordDbo m where m.country = :country")
    Float totalCost(String country);

}
