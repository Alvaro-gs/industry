package com.gestamp.industry.infrastructure.db.springdata.repository.impl;

import com.gestamp.industry.application.repository.UserRepository;
import com.gestamp.industry.domain.UserToken;
import com.gestamp.industry.infrastructure.db.springdata.mapper.UserMapper;
import com.gestamp.industry.infrastructure.db.springdata.repository.SpringDataUserRepository;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private final SpringDataUserRepository userRepository;

    private final UserMapper userMapper;

    public UserToken findByName(String name) {

        return userMapper.toDomain(userRepository.findByUsername(name));
    }



}
