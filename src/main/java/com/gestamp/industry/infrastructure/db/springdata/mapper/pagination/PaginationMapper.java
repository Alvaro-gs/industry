package com.gestamp.industry.infrastructure.db.springdata.mapper.pagination;

import com.gestamp.industry.domain.pagination.Pagination;
import com.gestamp.industry.infrastructure.rest.spring.mapper.parser.SortParser;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PaginationMapper {

  public static Pageable toPageable(Pagination pagination) {

    return pagination.getSortBy() == null
        ? PageRequest.of(pagination.getPageNumber(), pagination.getPageSize())

        : PageRequest.of(pagination.getPageNumber(), pagination.getPageSize(),
            SortParser.parse(pagination.getSortBy()));
  }

}
