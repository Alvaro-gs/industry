package com.gestamp.industry.infrastructure.db.springdata.mapper;

import com.gestamp.industry.domain.SalesRecord;
import com.gestamp.industry.infrastructure.db.springdata.model.SalesRecordDbo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SalesRecordMapper {

    SalesRecord toDomain(SalesRecordDbo dbo);

    SalesRecordDbo toInfrastructure(SalesRecord entity);

}
