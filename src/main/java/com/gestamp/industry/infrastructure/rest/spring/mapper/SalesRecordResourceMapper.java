package com.gestamp.industry.infrastructure.rest.spring.mapper;

import com.gestamp.industry.domain.SalesRecord;
import com.gestamp.industry.domain.TotalCost;
import com.gestamp.industry.infrastructure.rest.spring.dto.SalesRecordDto;
import com.gestamp.industry.infrastructure.rest.spring.dto.TotalCostDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SalesRecordResourceMapper {

  SalesRecordDto toDto(SalesRecord salesRecord);

  SalesRecord toDomain(SalesRecordDto salesRecordDtoo);

  TotalCostDto toDtoTotal(TotalCost totalCost);
}
