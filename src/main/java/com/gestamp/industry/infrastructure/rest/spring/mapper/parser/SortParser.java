package com.gestamp.industry.infrastructure.rest.spring.mapper.parser;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class SortParser {


  public static Sort parse(String sortBy) {
    Sort resultSort = null;
    String next;

    if (sortBy != null) {
      ArrayList<String> fields = new ArrayList<>(Arrays.asList(sortBy.split(",")));
      Iterator<String> iterator = fields.iterator();
      String field = iterator.next();
      while (field != null) {

        Sort sort = Sort.by(Sort.Direction.ASC, field);

        if (iterator.hasNext()) {
          next = iterator.next();
          if (next.equalsIgnoreCase(Direction.DESC.name()) || next.equalsIgnoreCase(Direction.ASC.name())) {
            sort = next.equalsIgnoreCase(Direction.DESC.name())
                ?  Sort.by(Direction.DESC, field)
                :  Sort.by(Direction.ASC, field);

            field = iterator.hasNext()
                ? iterator.next()
                : null;
          } else {
            field = next;
          }
        } else {
          field = null;
        }

        resultSort = resultSort == null
            ? sort
            : resultSort.and(sort);
      }
    }
    return resultSort;
  }

  public static ArrayList<String> getKeysSort(String sortBy) {
    Sort sort = SortParser.parse(sortBy);
    var result = new ArrayList<String>();
    sort.forEach((item) -> result.add(item.getProperty() + ":" + item.getDirection()));
    return result;
  }
}
