package com.gestamp.industry.infrastructure.rest.spring.config;

import com.gestamp.industry.application.exception.InternalServerException;
import com.gestamp.industry.application.exception.ResourceNotFoundException;
import com.gestamp.industry.infrastructure.rest.spring.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.naming.ServiceUnavailableException;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

@ControllerAdvice(basePackages = "com.gestamp.industry.infrastructure.rest.spring.resource")
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<ErrorDto> notFoundException(final ResourceNotFoundException e) {
    return createError(e, HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.getReasonPhrase());
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorDto> entityNotFoundException(final EntityNotFoundException e) {
    return createError(e, HttpStatus.BAD_REQUEST,
        HttpStatus.BAD_REQUEST.getReasonPhrase());
  }

  @ExceptionHandler(UnsupportedOperationException.class)
  public ResponseEntity<ErrorDto> unsupportedOperationException(final UnsupportedOperationException e) {
    return createError(e, HttpStatus.FORBIDDEN, HttpStatus.FORBIDDEN.getReasonPhrase());
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<ErrorDto> argumentException(final IllegalArgumentException e) {
    return createError(e, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ErrorDto> constraintViolationException(final ConstraintViolationException e) {
    return createError(e, HttpStatus.BAD_REQUEST, e.toString());
  }

  @ExceptionHandler(InternalServerException.class)
  public ResponseEntity<ErrorDto> internalServerError(final ServiceUnavailableException e) {
    return createError(e, HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
  }

  @ExceptionHandler(ServiceUnavailableException.class)
  public ResponseEntity<ErrorDto> serviceUnavailableException(final ServiceUnavailableException e) {
    return createError(e, HttpStatus.SERVICE_UNAVAILABLE, HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase());
  }

  private ResponseEntity<ErrorDto> createError(final Exception exception, final HttpStatus httpStatus,
      final String type) {
    ErrorDto error = getErrorDto(exception, httpStatus, type);

    return new ResponseEntity<>(error, httpStatus);
  }

  private ErrorDto getErrorDto(Exception exception, HttpStatus httpStatus, String type) {
    final String message =
        Optional.ofNullable(exception.getMessage()).orElse(exception.getClass().getSimpleName());

    ErrorDto error = new ErrorDto();
    error.setCode(httpStatus.value());
    error.setMessage(message);
    error.setType(type);
    return error;
  }
}
