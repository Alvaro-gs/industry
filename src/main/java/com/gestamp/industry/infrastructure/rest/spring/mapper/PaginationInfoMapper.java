package com.gestamp.industry.infrastructure.rest.spring.mapper;

import com.gestamp.industry.domain.pagination.PaginationInfo;
import com.gestamp.industry.infrastructure.rest.spring.dto.PaginationInfoDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PaginationInfoMapper {

  PaginationInfoDto toDto(PaginationInfo paginationInfo);

}
