package com.gestamp.industry.infrastructure.rest.spring.resource;

import com.gestamp.industry.application.service.SalesRecordService;
import com.gestamp.industry.domain.SalesRecord;
import com.gestamp.industry.domain.filter.SaleRecordFilter;
import com.gestamp.industry.domain.pagination.PageabledResult;
import com.gestamp.industry.domain.pagination.Pagination;
import com.gestamp.industry.infrastructure.rest.spring.dto.PageabledResultDto;
import com.gestamp.industry.infrastructure.rest.spring.dto.SalesRecordDto;
import com.gestamp.industry.infrastructure.rest.spring.dto.TotalCostDto;
import com.gestamp.industry.infrastructure.rest.spring.mapper.PaginationInfoMapper;
import com.gestamp.industry.infrastructure.rest.spring.mapper.SalesRecordResourceMapper;
import com.gestamp.industry.infrastructure.rest.spring.spec.SalesApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Slf4j
public class SalesApiResource implements SalesApi {

    private final SalesRecordService salesRecordService;

    private final SalesRecordResourceMapper salesRecordResourceMapper;

    private final PaginationInfoMapper paginationInfoMapper;

    @Override
    public ResponseEntity<SalesRecordDto> createSalesRecord(@Valid SalesRecordDto sales) {
        var result = salesRecordService.createSalesRecord(salesRecordResourceMapper.toDomain(sales));
        return ResponseEntity.ok(salesRecordResourceMapper.toDto(result));
    }

    @Override
    public ResponseEntity<SalesRecordDto> updateSalesRecord(@Valid SalesRecordDto sales) {
        var result = salesRecordService.updateSalesRecord(salesRecordResourceMapper.toDomain(sales));
        return ResponseEntity.ok(salesRecordResourceMapper.toDto(result));
    }

    @Override
    public ResponseEntity deleteSalesRecord(Long id) {
        salesRecordService.deleteSalesRecord(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<PageabledResultDto> findSalesRecorWithFilter(Long id, String region, String country,
        String itemType, String salesChannel, String orderPriority, String orderDate, Long orderId, String shipDate,
        Integer pageNumber, Integer pageSize, String sort) {

        var saleRecordFilter = SaleRecordFilter.builder()
                .id(id)
                .region(region)
                .country(country)
                .itemType(itemType)
                .salesChannel(salesChannel)
                .orderPriority(orderPriority)
                .orderDate(orderDate)
                .orderId(orderId)
                .shipDate(shipDate)
                .build();

        var pagination = Pagination.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .sortBy(sort)
                .build();

        PageabledResult<SalesRecord> pageableResult = salesRecordService
                .findSalesRecorWithFilter(saleRecordFilter, pagination );

        var salesRecords = pageableResult.getContent()
                .stream()
                .map(salesRecordResourceMapper::toDto)
                .collect(Collectors.toList());

        PageabledResultDto pageabledResultDto = new PageabledResultDto();
        pageabledResultDto.setPaginationInfo(paginationInfoMapper.toDto(pageableResult.getPaginationInfo()));
        pageabledResultDto.setContent(salesRecords);

        return new ResponseEntity<>(pageabledResultDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<TotalCostDto> operationTotalCost(@NotNull String country) {
        var result =  salesRecordService.operationTotalCost(country);
        return ResponseEntity.ok(salesRecordResourceMapper.toDtoTotal(result));

    }
}
