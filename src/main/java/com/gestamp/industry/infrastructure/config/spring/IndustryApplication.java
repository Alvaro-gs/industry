package com.gestamp.industry.infrastructure.config.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.gestamp.industry.infrastructure")
public class IndustryApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndustryApplication.class, args);
	}

}
