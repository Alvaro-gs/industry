package com.gestamp.industry.infrastructure.config.spring;

import com.gestamp.industry.application.repository.SalesRecordRepository;
import com.gestamp.industry.application.repository.UserRepository;
import com.gestamp.industry.application.service.SalesRecordService;
import com.gestamp.industry.application.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class IndustryConfiguration {

    private final SalesRecordRepository salesRecordRepository;

    private final UserRepository userRepository;

    @Bean
    public SalesRecordService salesRecordService() {
        return new SalesRecordService(salesRecordRepository);
    }

    @Bean
    public UserService userService() {
        return new UserService(userRepository);
    }


}
