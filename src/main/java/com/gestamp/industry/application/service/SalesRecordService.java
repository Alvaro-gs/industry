package com.gestamp.industry.application.service;

import com.gestamp.industry.application.repository.SalesRecordRepository;
import com.gestamp.industry.domain.SalesRecord;
import com.gestamp.industry.domain.TotalCost;
import com.gestamp.industry.domain.filter.SaleRecordFilter;
import com.gestamp.industry.domain.pagination.PageabledResult;
import com.gestamp.industry.domain.pagination.Pagination;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class SalesRecordService {

   private final  SalesRecordRepository salesRecordRepository;

    public SalesRecord createSalesRecord(SalesRecord salesRecord){
        return  salesRecordRepository.saveAndUpdateSalesRecord(salesRecord);
    }

    public SalesRecord updateSalesRecord(SalesRecord salesRecord){
        return  salesRecordRepository.saveAndUpdateSalesRecord(salesRecord);
    }

    public void  deleteSalesRecord(Long id) {
        salesRecordRepository.deleteSalesRecord(id);
    }

    public PageabledResult<SalesRecord> findSalesRecorWithFilter(SaleRecordFilter saleRecordFilter,
                                                                 Pagination pagination) {

      return  salesRecordRepository.findByFilter(saleRecordFilter, pagination);
    }

    public TotalCost operationTotalCost(String country) {
        Float result = salesRecordRepository.totalCost(country);
        return TotalCost.builder().total(result).build();
    }
}
