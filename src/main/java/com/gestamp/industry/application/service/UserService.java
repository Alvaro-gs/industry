package com.gestamp.industry.application.service;

import com.gestamp.industry.application.repository.UserRepository;
import com.gestamp.industry.domain.UserToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static java.util.Collections.emptyList;

@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {

   private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserToken userToken = userRepository.findByName(username);
        if (userToken == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(userToken.getUsername(), userToken.getPassword(), emptyList());
    }


}
