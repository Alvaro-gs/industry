package com.gestamp.industry.application.repository;

import com.gestamp.industry.domain.SalesRecord;
import com.gestamp.industry.domain.filter.SaleRecordFilter;
import com.gestamp.industry.domain.pagination.PageabledResult;
import com.gestamp.industry.domain.pagination.Pagination;

public interface SalesRecordRepository  {

    SalesRecord saveAndUpdateSalesRecord(SalesRecord salesRecord);

    void deleteSalesRecord(Long id);

    PageabledResult<SalesRecord> findByFilter(SaleRecordFilter filter, Pagination pagination);

    Float totalCost(String country);

}
