package com.gestamp.industry.application.repository;

import com.gestamp.industry.domain.UserToken;

public interface UserRepository {

    UserToken findByName(String name);

}
