package com.gestamp.industry.domain.filter;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SaleRecordFilter {

  private Long id;
  private String region;
  private String country;
  private String itemType;
  private String salesChannel;
  private String orderPriority;
  private String orderDate;
  private Long orderId;
  private String shipDate;

}
