package com.gestamp.industry.domain.pagination;

import lombok.Data;
import java.util.List;

@Data
public class PageabledResult<T> {

  private List<T> content;

  private PaginationInfo paginationInfo;
}
