package com.gestamp.industry.domain.pagination;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pagination {

  public static String SEPARATOR = ",";

  private Integer pageNumber;

  private Integer pageSize;

  private String sortBy;

}
