package com.gestamp.industry.domain.pagination;

import com.gestamp.industry.infrastructure.rest.spring.mapper.parser.SortParser;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
public class PaginationInfo<T> {

  private Boolean sorted;

  private Integer pageSize;

  private Integer pageNumber;

  private Long totalElements;

  private Integer totalPages;

  private List<String> sort;

  public PaginationInfo(Integer pageNumber, Integer pageSize, Page<T> page, String sortBy) {
    this.totalPages = page.getTotalPages();
    this.totalElements = page.getTotalElements();
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.sort = sortBy != null ? SortParser.getKeysSort(sortBy) : null;
  }
}
