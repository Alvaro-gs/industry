package com.gestamp.industry.domain;

import lombok.Data;


@Data
public class UserToken {

    private String username;
    private String password;

}
