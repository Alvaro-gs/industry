package com.gestamp.industry.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TotalCost {

    private Float total;

}
