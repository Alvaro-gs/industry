package com.gestamp.industry.domain;

public enum OrderPriorityEnum {

    L("L"),
    M("M"),
    H("H"),
    C("C");

    private String value;

    OrderPriorityEnum(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}
