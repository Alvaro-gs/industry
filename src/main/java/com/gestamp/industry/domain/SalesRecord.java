package com.gestamp.industry.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import java.time.LocalDate;

@Getter
@Setter
@Slf4j
public class SalesRecord {

    private Long id;

    private String region;

    private String country;

    private String itemType;

    private String salesChannel;

    private OrderPriorityEnum orderPriority;

    private String orderDate;

    private Long orderId;

    private LocalDate shipDate;

    private Long unitSold;

    private Float unitPrice;

    private Float unitCost;

    private Double totalRevenue;

    private Double totalCost;

    private Double totalProfit;

}
