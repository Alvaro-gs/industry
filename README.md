
# Getting started

1.) Levantamos un postgres

```bash
sudo docker run --rm --name local-postgres -e POSTGRES_PASSWORD=postgres  -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres

```

2.) Ejecutamos el script que hay en resources/db/script.sql para crear el schema y los datos base en el schema public

3.) Levantamos el micro

```bash
mvn spring-boot:run -f pom.xml
```

4.) Obtenemos el token con el endpoint /login

```bash
curl --location --request POST 'http://localhost:8080/login' \
--header 'Content-Type: application/json' \
--data-raw '{ "username": "admin", "password": "admin"}'
```

5.) En el api.yaml podemos ver el contrato de la api

6.) example call

http://localhost:8080/sales?pageSize=5&sort=country,desc
http://localhost:8080/sales/totalcost/?country=Andorra

